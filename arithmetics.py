# arithmetics

a = 10
b = 3

# + operation
hasil = a + b
print(a,'+',b,'=',hasil)

# - operation
hasil = a - b
print(a,'-',b,'=',hasil)

# x operation
hasil = a * b
print(a,'x',b,'=',hasil)

# / operation
hasil = a / b
print(a,'/',b,'=',hasil)

# exponen (pangkat) operation
hasil = a ** b
print(a,'^',b,'=',hasil)

# modulus (sisa bagi) operation
hasil = a % b
print(a,'%',b,'=',hasil)

# floor division operation
hasil = a // b
print(a,'//',b,'=',hasil)

# priority operation
'''
1. ()
2. exponential **
3. perkalian * / ** % //
4. pertambahan + -
'''
x = 3
y = 2
z = 4
hasil = x ** y * (z + x) / y - y % z // x
print(x,'**',y,'*',z,'+',x,'/',y,'-',y,'%',z,'//',x," = ",hasil)

hasil = x + y * z
print(x,'+',y,'*',z,'=',hasil)

hasil = (x + y) * z
print('(',x,'+',y,')','*',z,'=',hasil)