# comparation operation
# every comparation resulr is boolean
# >, <, >=, <=, ==, !=, is, is not

a = 4
b = 2

# lebih besar dari >
print('\nlebih besar dari >')
hasil = a > 3
print(a,'>',3,'=',hasil)
hasil = b > 3
print(b,'>',3,'=',hasil)

# lebih kecil dari <
print('\nlebih kecil dari <')
hasil = a < 3
print(a,'<',3,'=',hasil)
hasil = b < 3
print(b,'<',3,'=',hasil)

# lebih besar sama dengan dari >=
print('\nlebih besar sama dengan dari >=')
hasil = a >= 3
print(a,'>=',3,'=',hasil)
hasil = b >= 3
print(b,'>=',3,'=',hasil)

# lebih kecil sama dengan dari <=
print('\nlebih kecil sama dengan dari <=')
hasil = a <= 3
print(a,'<=',3,'=',hasil)
hasil = b <= 3
print(b,'<=',3,'=',hasil)

# Sama dengan dari ==
print('\nSama dengan dari ==')
hasil = a == 3
print(a,'==',3,'=',hasil)
hasil = b == 3
print(b,'==',3,'=',hasil)

# Bukan sama dengan dari !=
print('\nBukan sama dengan dari !=')
hasil = a != 3
print(a,'!=',3,'=',hasil)
hasil = b != 3
print(b,'!=',3,'=',hasil)

# 'Is" sebagai komparasi object identity
print('\nKomparasi is')
x = 5 # assigment membuat object
y = 5
print('nilai x = ',x,'id = ',hex(id(x)))
print('nilai y = ',y,'id = ',hex(id(y)))
hasil = x is y
print('x is y = ',hasil)

# 'Is not" sebagai komparasi object identity
print('\nKomparasi is not')
x = 5 # assigment membuat object
y = 6
print('nilai x = ',x,'id = ',hex(id(x)))
print('nilai y = ',y,'id = ',hex(id(y)))
hasil = x is not y
print('x is not y = ',hasil)
