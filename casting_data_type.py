# casting
# merubah dari satu tipe data ke tipe lain
# tipe data = int, float, str, bool

print("***int***")
data_int = 9;
print("data = ", data_int, ",type = ", type(data_int))

data_float = float(data_int)
data_str = str(data_int)
data_bool = bool(data_int) # false if int == 0
print("data = ", data_float, ",type = ", type(data_float))
print("data = ", data_str, ",type = ", type(data_str))
print("data = ", data_bool, ",type = ", type(data_bool))

# data ffloatt
print("***float***")
data_float = 9.5;
print("data = ", data_float, ",type = ", type(data_float))

data_int = int(data_float) # dibulatkan ke bawah
data_str = str(data_float)
data_bool = bool(data_float) # false if int == 0
print("data = ", data_int, ",type = ", type(data_int))
print("data = ", data_str, ",type = ", type(data_str))
print("data = ", data_bool, ",type = ", type(data_bool))

# data bolean
print("***float***")
data_bool = False;
print("data = ", data_bool, ",type = ", type(data_bool))

data_int = int(data_bool) # dibulatkan ke bawah
data_str = str(data_bool)
data_float = float(data_bool) # false if int == 0
print("data = ", data_int, ",type = ", type(data_int))
print("data = ", data_str, ",type = ", type(data_str))
print("data = ", data_bool, ",type = ", type(data_bool))

# data string
print("***float***")
data_str = "10";
print("data = ", data_str, ",type = ", type(data_str))

data_int = int(data_str) # dibulatkan ke bawah
data_float = float(data_str)
data_bool = bool(data_str) # false if int == 0
print("data = ", data_int, ",type = ", type(data_int))
print("data = ", data_float, ",type = ", type(data_float))
print("data = ", data_bool, ",type = ", type(data_bool))