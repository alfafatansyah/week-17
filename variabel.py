# variabel adalah tempat menyimpan data

# menaruh / assignment nilai
a = 10
x = 5
panjang = 1000

print("Nilai a = ", a)
print("Nilai x = ", x)
print("Nilai panjang = ", panjang)

# penamaan
nilai_y = 15 # dengan menggunakan underscore
juta10 = 10000000 # angka setelah huruf
nilaiZ = 17.5 # menggunakan Sentence Case

#pemanggilan kedua
print("Nilai a = ", a)
a = 7
print("Nilai a = ", a)

# assugbnebt indirect
b = a
print("Nilai b = ", b)
