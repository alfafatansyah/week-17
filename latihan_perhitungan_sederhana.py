# latihan konversi satuan temperature

# program konversi celcius ke satuan lain

print("\nPROGRAM KONVERSI TEMPERATU\n")

celcius = float(input('Masukkan suhu dalam celcius : '))
print("suhu adalah ", celcius, " Celcius")

# reamour
# (4/5)C
reamour = (4 / 5) * celcius
print("suhu dalam ", reamour, " Reamour")

# fahrenheit
# (9/5)C + 32
fahrenheit = (9 / 5) * celcius + 32
print("suhu dalam ", fahrenheit, " Fahrenheit")

# kelvin
# C + 273
kelvin = celcius + 273
print("suhu dalam ", kelvin, " Kelvin")
