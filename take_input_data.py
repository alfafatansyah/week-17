#input user

# data yang dimasukan pasti string
data = input("masukkan data: ")
print("data = ", data, "type = ", type(data))

# jika ingin mengambil data int
data_int = int(input("masukkan data: "))
print("data = ", data_int, "type = ", type(data_int))

# jika ingin mengambil data boolean
data_boolean = bool(int(input("masukkan data: ")))
print("data = ", data_boolean, "type = ", type(data_boolean))